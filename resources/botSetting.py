# Tim Maba Palsu CFX - 2018

# Mungkin file ini ga usah di ubah aneh - aneh, kecuali memang mungkin perlu diubah
# Bagian ini hanya import saja

import errno
import os
import sys
import json
import tempfile
import requests

from flask import Flask, request, abort
from linebot import (LineBotApi, WebhookHandler)
from linebot.exceptions import (InvalidSignatureError, LineBotApiError)

# Bagian di bawah ini hanya mengimport module module yang dimiliki line_bot_sdk
# Misalnya "MessageEvent" itu digunakan untuk menghandle pesan yang masuk dari user
# Misalnya juga "SourceUser" itu digunakan untuk mendapatkan ID pengguna yang melakukan chat ke bot
# Kalau bingung, ini ada di Line Dev Reference https://developers.line.me/en/docs/messaging-api/reference/

from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
    SourceUser, SourceGroup, SourceRoom,
    TemplateSendMessage, ConfirmTemplate, MessageTemplateAction,
    ButtonsTemplate, ImageCarouselTemplate, ImageCarouselColumn, URITemplateAction,
    PostbackTemplateAction, DatetimePickerTemplateAction,
    CarouselTemplate, CarouselColumn, PostbackEvent,
    StickerMessage, StickerSendMessage, LocationMessage, LocationSendMessage,
    ImageMessage, VideoMessage, AudioMessage, FileMessage,
    UnfollowEvent, FollowEvent, JoinEvent, LeaveEvent, BeaconEvent
)

# Inisiasi menggunakan module Flask, jadi kodingan ini menggunakan Flask sebagai module utamanya
app = Flask(__name__)

# Bagian ini untuk mengakses bot menggunakan token, token dalam Line Dev itu ada CHANNEL_SECRET dan CHANNEL_ACCESS_TOKEN
# Bagian ini sudah dari sananya, jadi kalau bisa stringnya jangan dimodifikasi aneh - aneh, karena tokennya sendiri udah di masukkan ke heroku

channel_secret = os.getenv('LINE_CHANNEL_SECRET', None)
channel_access_token = os.getenv('LINE_CHANNEL_ACCESS_TOKEN', None)
if channel_secret is None:
    print('Gagal! LINE_CHANNEL_SECRET Belum di Setting di Heroku')
    sys.exit(1)
if channel_access_token is None:
    print('Gagal! LINE_CHANNEL_ACCESS_TOKEN Belum di Setting di Heroku')
    sys.exit(1)

# SEHATCHAN ini adalah objeknya, nanti objek ini akan selalu digunakan
SEHATCHAN = LineBotApi(channel_access_token)

# ini webhook handler, jadi yang menghandle trigger respon dari bot ke server heroku
handler = WebhookHandler(channel_secret)

# Bagian bawah ini kalau bisa jangan dimodifikasi, ini udah dari sananya.

@app.route("/callback", methods=['POST'])
def callback(): # Fungsi ini memanggil webhook

    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)

    return 'OK'