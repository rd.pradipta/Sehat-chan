# Tim Maba Palsu CFX - 2018
# Dibuat dengan </> dan <3

# Main program

# Jangan lupa selalu pakai command "heroku logs" setiap selesai deploy (buat liat programnya crash / nggak).
# Kalau crash, berarti ada yang salah programnya & bot nya pun ga bisa dites kalau crash

from aabMain import *
from dipsiMain import *

# Setiap interaksi yang terjadi di bot, dianggap sebagai sebuah event
# Setiap event mempunya token aktivitasnya sendiri - sendiri sebagai identitas dari event itu, tapi tak perlu dipikirkan karena itu otomatis

# Bagian bawah ini menghandle pesan masuk jika bot menerima dalam bentuk pesan TEKS (message=TextMessage)
# "MessageEvent" maksudnya bisa dibilang adalah tujuan module nya, terus "TextMessage" itu kalau misalnya bot menerima pesan berupa TEKS
# Pesan yang diterima bot kan bisa bermacam - macam, bisa berupa text, berupa gambar, berupa stiker, dan lain - lainnya
# Nama def/function nya, sudah ada dari sananya

@handler.add(MessageEvent, message=TextMessage)
def handle_text_message(event):

    # Variabel "masukan" adalah pesan dari user yang masuk, dan berupa STRING
    # event.message.text boleh dianggap sebagai input("") kalau biasanya di Python untuk minta input dari User
    
    masukan_raw = event.message.text
    masukan = masukan_raw.lower()
    masukan_splitted = masukan.split()

    # Fungsi untuk memberikan respon berupa TEKS
    # Parameter "pesan" isinya adalah STRING yang mau dibalas
    # Method reply_message (penamaan udah dari sananya) untuk memberikan balasan
    # event.reply_token (penamaan udah dari sananya) menyatakan token dari event... event balas pesan 
    # TextSendMessage (penamaan udah dari sananya), menyatakan balasan yang dikirim berupa pesan teks
    # (text = pesan) maksudnya adalah, pesan apa yang mau dibalas
    
    def replyTxt(pesan):
        SEHATCHAN.reply_message(event.reply_token,TextSendMessage(text=pesan))

    # Bagian ini digunakan untuk mengambil ID dari user yang chat ke bot, supaya bot bisa tahu yang chat siapa
    # Jadi, LINE menganggap setiap pesan dan interaksi yang masuk ke bot itu adalah sebagai suatu event 

    # isinstance di bawah ini untuk cek apakah sumber dari event tersebut berasal dari GRUP ("SourceGroup")
    # variabel "pengirim" adalah si user yang chat ke bot
    # variabel "sumberAsal" adalah wilayah sumber pesan
    # Syntax nya memang sudah dari sananya begitu

    if isinstance(event.source, SourceGroup):
        pengirim = SEHATCHAN.get_group_member_profile(event.source.group_id, event.source.user_id)
        sumberAsal = event.source.group_id

    # isinstance di sini untuk cek apakah sumbernya dari MULTICHAT ("SourceRoom")
    elif isinstance(event.source, SourceRoom):
        pengirim = SEHATCHAN.get_room_member_profile(event.source.room_id, event.source.user_id)
        sumberAsal = event.source.room_id
    
    # isintance ini untuk cek apakah sumbernya dari private chat ke bot 
    else:
        pengirim = SEHATCHAN.get_profile(event.source.user_id)
        sumberAsal = event.source.user_id

    # _______________________________________________________________________________________________ #
    # Saatnya eksekusi bot sesuai keyword yang diinginkan
    # Sekarang penggunaan udah bisa bebas, tapi syntax tetap harus sesuai ketentuan di SDK nya...


    # Bagian ini adalah contoh respon kalau user memasukkan kata "test" ke bot

    if masukan == "test":
        replyTxt("Aku sudah aktif! Ada yang bisa dibantu ?")   

    # Bagian ini adalah contoh respon kalau user memasukkan kata "help" ke bot

    elif masukan == "help":
        replyTxt(bantuanUmum)   # Variabel "bantuanUmum" ada di file botHelp.py
            
    else:
        # Ini misalnya bot respon apabila input yang diberikan selain yang sudah terdaftar
        # Supaya ga spam :v

        return "OK"



# Bagian ini sebenarnya tak perlu dimodifikasi
# Bagian ini hanya bertugas menghubungkan bot dengan server, ini sudah ada dari sananya

if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)