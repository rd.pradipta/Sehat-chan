## Seh-Chan (Sehat Chan)
  
In Sya Allah Bot untuk Lomba CFX ChatBot Invasion  
Fitur yang kemungkinan ada:  
  
1. Fitur Smart Living:
    - Mengingatkan minum air
    - Mengingatkan berolahraga
    - Daftar menu sehat
    - Mode biasa & mode diet
    - Menu sehat
    - Pengingat fitness / olahraga
    - Olahraga harian yang bermanfaat
    - Penampil & perhitungan gizi hidangan  
  
2. Fitur Smart Mobility:
	- Jalan - jalan tanpa tujuan
	- Suggest tempat hiburan terdekat
	- Pendeteksi jalan macet  
  
3. Miscellanous:  
  	- Kalkulator
  	- Cuaca daerah yang diinginkan
  	- Penampil berita
  	- Terjemahan bahasa
  	- Adzan harian
  	- Soal Solver

# Dikembangkan oleh:  
Tim Maba Palsu  
1. Aab (Muhamad Abdurahman)  
2. Dipsi  (Rd Pradipta Gitaya S)
3. Sidiq (Usman Sidiq)